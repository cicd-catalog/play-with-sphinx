# Play with Sphinx

Ce modèle de CI-CD vous permet d'automatiser vos inscriptions au [SUAPS](https://www.umontpellier.fr/campus/sport), quelle que soit votre discipline, votre prof, ou votre couleur de cheveux

## Fonctionnement

Ce modèle tire parti des fonctionnalités de [pipeline scheduling](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) proposées par GitLab, pour vous enregistrer à un activité via une requête cURL récurrente.


## Arguments

Les liens d'inscription sont composés comme suit :

```
https://sphinx.umontpellier.fr/SurveyServer/s/SUAPS/<NOM PROF>/questionnaire
```

| Paramètre | Contenu | Description |
|-----------|---------|-------------|
| `nomProf`* | Nom en MAJUSCULES | **Nom du prof**, qui sera placé dans l'URL indiquée ci-dessus |
| `nom`*  | Libre |  **Nom de famille** à renseigner dans le formulaire |
| `prenom`* | Libre | **Prénom** à renseigner dans le formulaire |
| `statut` | `etudiant` <br> `personnel` | **Réponse à la question *"Je suis"*** du questionnaire ![](assets/i_am_question.png) |
| `universite` | `montpellier` <br> `paul_valery` | **Réponse à la question *"Je suis à l'"*** du questionnaire. ![](assets/i_work_in_question.png) |

<details>
<summary>Options avancées</summary>

| Paramètre | Contenu | Description |
|-----------|---------|-------------|
| `dryRun`  | `true`/`false` | **Désactive l'envoi de la requête**, ne laissant que le rapport à la fin de son exécution |
| `branche` | Nom de branche (`main`) <br> ID de commit (`63f3168...`) | **Force l'utilisation d'une version** précise du script de requête |

</details>
<br>

_*Option requise_

## Configuration et mise en place

### Utilisation du composant CI/CD

Pour utiliser ce composant, incluez-le dans votre pipeline CI/CD en intégrant le bloc suivant :

```yaml
include:
  # 1: incluez le composant
  - component: $CI_SERVER_FQDN/cicd-catalog/play-with-sphinx/play-with-sphinx@<version>
    # 2: définissez vos options
    inputs:
      nomProf: NOMPROF
      prenom: John
      nom: Doe
      statut: etudiant
      universite: montpellier

```

<details>
<summary><strong>Inscription multi-cours</strong></summary>

Si vous souhaitez planifier l'inscription à des cours différents (= avec des profs différents), vous pouvez [généraliser](https://fr.wikipedia.org/wiki/G%C3%A9n%C3%A9ralisation#En_informatique) le pipeline en modifiant la ligne `nomProf` avec :

```yaml
nomProf: $SCHEDULED_PROF
```

Puis suivre les instructions appropriées dans la partie suivante
</details>

### Planification de l'inscription

Enfin, pour automatiser l'inscription, créez des [Pipeline Schedules](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) dans votre dépôt :

1. Ouvrez la page des **Pipeline Schedules**

![](assets/pipeline_schedules_menu.png)

2. Créez une planification en renseignant les informations suivantes :
   * **Description** _(optionnel)_ : indiquez à quel cours vous vous inscrivez
   * **Intervalle** : Sélectionnez _Personnalisé_ et aidez-vous de ce site pour savoir ce que vous devez mettre dans la zone de texte
   * **Fuseau horaire** : Sélectionnez _[UTC+2] Paris_

![](assets/new_schedule.png)

<details>
<summary><strong>Inscription multi-cours</strong></summary>

Dans le cas d'une inscription à plusieurs cours (et sous réserve que vous ayiez suivi la partie dédiée dans la section précédente), vous pouvez attribuer une valeur spécifique à la variable `$SCHEDULED_PROF` en fonction des planifications en définissant la variable dans la partie **Variables** :

![](assets/scheduled_variables.png)

</details>
<br>

3. Confirmez la création du pipeline

> **REMARQUE :** Si vous êtez absent(e) à un cours, au lieu de supprimer le pipeline dans sa totalité, modifiez-le et décochez l'option _Activé_

## Licence

Ce code est soumis à la licence [GNU General Public License v3](LICENSE)


## Auteur

Conçu avec ❤️ et 😰 par [Dorian Blanchet (@blanchetd)](https://gitlabinfo.iutmontp.univ-montp2.fr/blanchetd)