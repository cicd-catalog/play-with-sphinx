import subprocess
import requests
import markdown
import sys
from datetime import datetime
import os
from bs4 import BeautifulSoup

if not ('NOM_PROF' in os.environ and 'NOM' in os.environ and 'PRENOM' in os.environ):
    if 'NOM_PROF' not in os.environ:
        print("Erreur : Aucun professeur n'a été renseigné !")
    if 'NOM' not in os.environ:
        print("Erreur : Aucun nom de famille n'a été renseigné !")
    if 'PRENOM' not in os.environ:
        print("Erreur : Aucun prénom n'a été renseigné !")
    exit(2)

statusDict = {
    "etudiant":"1",
    "personnel":"2"
}

universiteDict = {
    "montpellier": "1",
    "paul_valery": "2"
}

# Download the HTML content
url = f"https://sphinx.umontpellier.fr/SurveyServer/s/SUAPS/{os.environ.get('NOM_PROF')}/questionnaire.htm"

response = requests.get(url)
html_content = response.text

# Extract cookies from the response headers
cookies = response.cookies.get_dict()

# Parse the HTML and extract form data
soup = BeautifulSoup(html_content, "html.parser")
form = soup.find("form")

# Collect form parameters & fill required ones
form_data = {}

def save_param(key: str, value: str):
    fixedValue = (None, value)
    form_data[key] = fixedValue
    return

for input_field in form.find_all("input"):
    name = input_field.get("name")
    value = input_field.get("value")
    if name:
        match name:
            case "1052199243": # UNIVERSITÉ
                save_param(name, universiteDict[os.environ.get('UNIVERSITE')])
            case "843888400":  # ETUDIANT/PERSONNEL
                save_param(name, statusDict[os.environ.get('STATUT')])
            case "313906923":  # PRÉNOM
                save_param(name, os.environ.get('PRENOM'))
            case "262832807":  # NOM
                save_param(name, os.environ.get('NOM'))
            case _:
                if value:
                    save_param(name, value)

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:121.0) Gecko/20100101 Firefox/121.0',
    'Accept': '*/*',
    'Accept-Language': 'fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3',
    'Referer': url,
    'X-Requested-With': 'XMLHttpRequest',
    'Origin': 'https://sphinx.umontpellier.fr',
    'DNT': '1',
    'Connection': 'keep-alive',
    'Sec-Fetch-Dest': 'empty',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Site': 'same-origin',
    'Pragma': 'no-cache',
    'Cache-Control': 'no-cache',
}

md_output_1 = f"""# Détails de la requête d'inscription
Rapport de la requête d'inscription initiée par le pipeline {os.environ.get('CI_PIPELINE_IID')}

{"> **NOTE** : Exécuté en dry-run" if os.environ.get('DRY_RUN') == 'true' else ""}

## Propriétés du pipeline

**Pipeline ID :** {os.environ.get('CI_PIPELINE_IID')}

**Pipeline name :** {os.environ.get('CI_PIPELINE_NAME')}

**Nom du projet :** [{os.environ.get('CI_PROJECT_NAME')}]({os.environ.get('CI_PROJECT_URL')})

**Version du runner :** {os.environ.get('CI_RUNNER_VERSION')}

**Type d'exécution :** {"Manuel" if 'CI_JOB_MANUAL' in os.environ else "Automatique/Planifié"}

**Heure d'exécution :** {datetime.now().strftime("%m/%d/%Y à %H:%M:%S")}

## Options sélectionnées

**Professeur détecté :** {soup.find("div", attrs={"id":"header"}).find("td", attrs={"class":"title-style"}).text}

**Nom :** {os.environ.get('NOM')}

**Prénom :** {os.environ.get('PRENOM')}

{("**Statut :** " + os.environ.get('STATUT').title()) if 'STATUT' in os.environ else ""}

{("**Université :** " + os.environ.get('UNIVERSITE').replace('_',' ').title()) if 'UNIVERSITE' in os.environ else ""}
"""

raw_output_1 = """<details>
<summary style="font-weight: 700; font-size: 20;">Informations avancées</summary>
"""

md_output_2 = f"""### En-têtes de la requête

{"\n\n".join([f"**{key} :** `{value}`" for key, value in headers.items()])}

### Cookies

{"\n\n".join([f"**{key} :** `{value}`" for key, value in cookies.items()])}

### Données de formulaire

{"\n\n".join([f"**{key} :** `{value}`" for key, value in form_data.items()])}
"""

raw_output_2 = "</details>"

md_output_3 = """## À propos de ce rapport

Ce document a été généré par [Play With Sphinx](https://gitlabinfo.iutmontp.univ-montp2.fr/blanchetd/play-with-sphinx)

Créé par [Dorian Blanchet](https://gitlabinfo.iutmontp.univ-montp2.fr/blanchetd)
"""
with open('rapport.html', 'wb') as f:
    f.write(bytes(markdown.markdown(md_output_1) + raw_output_1 + markdown.markdown(md_output_2) + raw_output_2 + markdown.markdown(md_output_3), "UTF-8"))

if os.environ.get('DRY_RUN') != 'true':
    response = requests.post(
        url,
        cookies=cookies,
        headers=headers,
        files=form_data,
    )
    with open('output.html', 'wb') as f:
        f.write(response.content)

print("Terminé !")